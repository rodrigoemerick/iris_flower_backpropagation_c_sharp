﻿//Example developed by Rodrigo de Jesus Emerick to free use of study or as a basis to other implementations, feel free to use this project for any purposes

using System;
using System.IO;
using System.Globalization;
using System.Collections.Generic;

namespace IrisFlower_Backpropagation
{
    class Program
    {
        static void Main(string[] args)
        {
            #region CONST VARIABLES
            //The two lines below force the decimal numbers separator to be a dot, as the default Portuguese separator is a comma, converting the decimal numbers of the file to doubles would generate an error, so forcing it from the beggining should solve this issue
            NumberFormatInfo number = (NumberFormatInfo)CultureInfo.CurrentCulture.NumberFormat.Clone();
            number.NumberDecimalSeparator = ".";

            const int m_QtyHidden = 5; //Number of neurons on hidden layer
            const int m_QtyOutput = 3; //Number of neurons on output layer
            const int m_NumOfGens = 50000; //Number of max generations in case Mean Squared Error condition is not satisfied
            const double m_LearningRate = 0.1; //Value of the learning rate of each training iteration, it's a value that works for this example

            string m_TrainFile = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.train.txt"; //Directory of the train file, it's together with the project .sln file
            string m_ValidateFile = Directory.GetCurrentDirectory() + "\\..\\..\\..\\..\\iris.test.txt"; //Directory of the validation file, it's together with the project .sln file
            #endregion

            string m_TryAgain = "yes";

            while (m_TryAgain != "no") //Until user types 'no' after the results, the program will execute repeatdly
            {
                #region MAIN VARIABLES
                //----------Perceptron training variables----------
                Random m_Random = new Random((int)DateTime.UtcNow.Ticks);
                List<double[]> m_Inputs = new List<double[]>(); //All inputs for one neuron
                List<double[]> m_IHWeights = new List<double[]>(); //Variable name abbreviating Inputs-Hidden weights
                List<double[]> m_HOWeights = new List<double[]>(); //Variable name abbreviating Hidden-Output weights

                const double m_Bias = 0.0; //In this example, the bias is not used so its value is 0, but to make the neural network tend to a result, put your value here
                const double m_MSEBreak = 0.05; //Value of Mean Squared Error to increase MSECounter if total MSE is lower than it
                const double m_Percentage = 0.6; //Value of train lines percentage to stop training if MSECounter is greater than it

                List<string> m_ExpectedResults = new List<string>(); //List containing the last column value of the files
                List<double> m_ConvertedResults = new List<double>(); //List containing the last column value of the files converted to numerical values

                List<double> m_HiddenLayerOutputs = new List<double>(); //Outputs of the hidden layer
                List<double> m_OutputLayerOutputs = new List<double>(); //Outputs of the output layer
                Neuron m_HiddenNeuron = new Neuron(); //This Neuron instance will calculate the output of the hidden layer neurons
                Neuron m_OutputNeuron = new Neuron(); //This Neuron instance will calculate the output of the output layer neurons
                #endregion

                #region ALGORITHM EXECUTION
                //----------Initializing weights with random values between 0.0 and 1.0----------
                RandomizeWeights();

                //----------Debugging weights before training----------
                Console.WriteLine("\nBefore training:\n");
                DebugWeights(m_IHWeights, m_HOWeights, false);

                //----------Training the neural network----------
                int currentGen = 0; //This variable controls the current generation of training, if it surpasses the max number of generations, the training is restarted
                ReadFile(m_TrainFile); //Populate the lists with the values of the training file to be used to train the neural network
                TrainNeuralNetwork(); //This function executes the feedforward and the backpropagation calculations

                //----------Debugging weights after training----------
                Console.WriteLine("\nAfter training:\n");
                DebugWeights(m_IHWeights, m_HOWeights, false);

                //----------Validating----------
                Console.WriteLine("\nResults of the test dataset (used to validate training):\n");
                ReadFile(m_ValidateFile);

                for (int i = 0; i < m_Inputs.Count; ++i)
                {
                    FeedForward(i, true);

                    for (int j = 0; j < m_OutputLayerOutputs.Count; j += m_QtyOutput)
                    {
                        Console.WriteLine($"Result: {ConvertToIrisName(m_OutputLayerOutputs[j])} | Should be: {m_ExpectedResults[i]}");
                    }
                }
                #endregion

                #region FUNCTIONS
                void RandomizeWeights()
                {
                    for (int i = 0; i < m_QtyHidden; ++i)
                    {
                        //Repeated four times because there are four inputs to the hidden layer
                        m_IHWeights.Add(new double[] { m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble() }); 
                    }

                    for (int i = 0; i < m_QtyOutput; ++i)
                    {
                        //Repeated five times because there are five inputs to the output layer
                        m_HOWeights.Add(new double[] { m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble(), m_Random.NextDouble() });
                    }
                }

                void DebugWeights(List<double[]> hidden, List<double[]> output, bool isRecalculating)
                {
                    //On both 'for' loops, if the max of generations is surpassed, debugs the new weights, else just prints the weights
                    for (int i = 0; i < hidden.Count; ++i)
                    {
                        for (int j = 0; j < hidden[i].Length; ++j)
                        {
                            Console.WriteLine(isRecalculating ? $"New Hidden Weight: {hidden[i][j]}" : $"Hidden weigth: {hidden[i][j]}");
                        }
                    }

                    Console.WriteLine();

                    for (int i = 0; i < output.Count; ++i)
                    {
                        for (int j = 0; j < output[i].Length; ++j)
                        {
                            Console.WriteLine(isRecalculating ? $"New Output Weight: {output[i][j]}" : $"Output weigth: {output[i][j]}");
                        }
                    }
                }

                void ReadFile(string path)
                {
                    //The three lines below clear the lists every time this function is called, in order not to cause the issue of having different lists values on different processes (training and validating)
                    m_Inputs.Clear();
                    m_ExpectedResults.Clear();
                    m_ConvertedResults.Clear();

                    string[] lines = File.ReadAllLines(path); //This function saves on each index of this array a line from the file
                    List<double> lineValues = new List<double>();

                    for (int i = 0; i < lines.Length; ++i)
                    {
                        lineValues.Clear();
                        string[] tempLine = lines[i].Split(","); //Temp array to store the current iterating line
                        double[] tempInputs = new double[tempLine.Length - 1]; //Temp array to store the first four columns of the file which are the numerical attributes

                        for (int j = 0; j < tempLine.Length - 1; ++j)
                        {
                            lineValues.Add(double.Parse(tempLine[j], number)); //Here the variable number is used as the separator of the conversion, so it's forcing the parsing of the decimal numbers to understand the dot as the decimal separator
                        }

                        lineValues.CopyTo(tempInputs); //As we're working with lists of arrays, we first copy the lineValues list to the tempInputs array and then add to the m_Inputs list
                        m_Inputs.Add(tempInputs);

                        m_ExpectedResults.Add(tempLine[^1]); //This is the same as tempLine[tempLine.Length - 1] | Adding the categorical last column value to the list of strings
                    }
                    m_ConvertedResults = ConvertExpected(m_ExpectedResults); //Converting the list of categorical values to numerical
                }

                void FeedForward(int inputListIndex, bool isValidating)
                {
                    //The two lines below clear the lists so they never use values of other neurons
                    m_HiddenLayerOutputs.Clear();
                    m_OutputLayerOutputs.Clear();

                    //Producing outputs of the neurons of the hidden layer
                    for (int i = 0; i < m_QtyHidden; ++i)
                    {
                        m_HiddenNeuron.SetNeuron(m_Inputs[inputListIndex], m_IHWeights[i], m_Bias);
                        m_HiddenLayerOutputs.Add(m_HiddenNeuron.Output);
                    }

                    //Producing outputs of the neurons of the output layer
                    for (int i = 0; i < m_QtyOutput; ++i)
                    {
                        m_OutputNeuron.SetNeuron(m_HiddenLayerOutputs, m_HOWeights[i], m_Bias);
                        m_OutputLayerOutputs.Add(isValidating ? m_OutputNeuron.Result(m_OutputNeuron.Output) : m_OutputNeuron.Output);
                    }
                }

                void TrainNeuralNetwork()
                {
                    while (true)
                    {
                        int mseCounter = 0; //If this counter surpasses 60% of the size of training file (that's because m_Percentage = 0.6), the training has been completed

                        for (int i = 0; i < m_Inputs.Count; ++i)
                        {
                            //----------Feed Forward----------
                            FeedForward(i, false);

                            //----------Backpropagation----------
                            //The following funcs are going to be used in the entire process of backpropagation
                            Func<double, double, double> DeltaError = (y, f) => y - f; //Delta used in the formula of error of output layer
                            Func<double, double> SigmoidDerivative = x => x * (1.0 - x); //As the name suggests, calculates the derivative of the sigmoid function
                            Func<double, double, double, double, double> UpdateWeight = (weight, learningRate, input, error) => weight + (learningRate * input * error); //Formula used to update the value of a weight

                            //----------Updating weights of the output<->hidden layer----------
                            List<double> deltaErrors = new List<double>();
                            List<double> derivativesOutputLayer = new List<double>(); //Derivatives of the output of the output layer neurons

                            double error = 0.0; //This is the current error of the current layer
                            double sumOfOutputLayers = 0.0; //This sum is used in the formula of calculating the error of the hidden layer (gradient descent)

                            for (int j = 0; j < m_OutputLayerOutputs.Count; ++j)
                            {
                                deltaErrors.Add(DeltaError(m_ConvertedResults[i], m_OutputLayerOutputs[j])); //Expected value - (minus) obtained value
                                derivativesOutputLayer.Add(SigmoidDerivative(m_OutputLayerOutputs[j])); //Calculating the sigmoid derivative of each output
                                error = derivativesOutputLayer[j] * deltaErrors[j]; //Current error of the output layer
                                sumOfOutputLayers += error; //Incrementing the variable for later use
                            }

                            //If the Mean Squared Error of this iteration is lower than the MSEBreak value, it should increase the MSECounter to use for breaking the execution
                            if (MSE(deltaErrors) < m_MSEBreak)
                            {
                                ++mseCounter;
                            }

                            //This 'for' is similar to the one above, but I wrote it again because the 'if' above is necessary to happen before the weight update
                            for (int j = 0; j < derivativesOutputLayer.Count; ++j)
                            {
                                for (int k = 0; k < m_HOWeights[j].Length; ++k)
                                {
                                    m_HOWeights[j][k] = UpdateWeight(m_HOWeights[j][k], m_LearningRate, m_HiddenLayerOutputs[k], error); //Updating weights
                                }
                            }

                            //----------Updating weights of the hidden<->input layer----------
                            List<double> derivativesHiddenLayer = new List<double>(); //Derivatives of the output of the hidden layer neurons

                            for (int j = 0; j < m_HiddenLayerOutputs.Count; ++j)
                            {
                                derivativesHiddenLayer.Add(SigmoidDerivative(m_HiddenLayerOutputs[j])); //Calculating the sigmoid derivative of each output
                            }

                            for (int j = 0; j < derivativesHiddenLayer.Count; ++j)
                            {
                                error = derivativesHiddenLayer[j] * sumOfOutputLayers; //Current error of the hidden layer

                                for (int k = 0; k < m_IHWeights[j].Length; ++k)
                                {
                                    m_IHWeights[j][k] = UpdateWeight(m_IHWeights[j][k], m_LearningRate, m_Inputs[i][k], error); //Updating weights
                                }
                            }
                        }

                        ++currentGen; //End of process, increases the current generation used as second condition to make sure the program won't enter in a infinite loop

                        if (mseCounter >= m_Inputs.Count * m_Percentage) //The Mean Squared Error was satisfied, the training is complete
                        {
                            break;
                        }
                        else if (currentGen >= m_NumOfGens) //Restart the training process if the Mean Squared Error condition has not been satisfied
                        {
                            Console.WriteLine("\nSomething got wrong. Generating new weights and training again...");
                            currentGen = 0;

                            m_IHWeights.Clear();
                            m_HOWeights.Clear();
                            RandomizeWeights();
                            DebugWeights(m_IHWeights, m_HOWeights, true);
                            TrainNeuralNetwork();

                            break;
                        }
                    }
                }
                #endregion

                Console.WriteLine("\nRun again? Type 'yes' or 'no'"); //Should the program restart or not?
                m_TryAgain = Console.ReadLine().ToLower();
            }

            Console.ReadKey();
        }

        #region STATIC FUNCTIONS
        //The numerical values converted in the function below serves well for this problem
        static List<double> ConvertExpected(List<string> expected)
        {
            List<double> converted = new List<double>();

            for (int i = 0; i < expected.Count; ++i)
            {
                switch (expected[i])
                {
                    case "Iris-setosa":
                        converted.Add(0.0); //considering the numerical value of iris-setosa as 0.0
                        break;
                    case "Iris-versicolor":
                        converted.Add(0.5); //considering the numerical value of iris-versicolor as 0.5
                        break;
                    case "Iris-virginica":
                        converted.Add(1.0); //considering the numerical value of iris-virginica as 1.0
                        break;
                }
            }

            return converted;
        }

        //The Mean Squared Error is the sum of the squared value of each error
        static double MSE(List<double> errors)
        {
            double mse = 0.0;

            for (int i = 0; i < errors.Count; ++i)
            {
                mse += errors[i] * errors[i];
            }

            return mse;
        }

        //This function converts the final output of the neural network back to the name of a flower to be printed at the end
        static string ConvertToIrisName(double result)
        {
            string converted = "";

            switch (result)
            {
                case 0.0:
                    converted = "Iris-setosa";
                    break;
                case 0.5:
                    converted = "Iris-versicolor";
                    break;
                case 1.0:
                    converted = "Iris-virginica";
                    break;
            }

            return converted;
        }
        #endregion
    }
}