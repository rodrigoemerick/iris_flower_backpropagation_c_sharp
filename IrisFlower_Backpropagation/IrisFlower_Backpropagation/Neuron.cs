﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IrisFlower_Backpropagation
{
    class Neuron
    {
        double[] m_Inputs;
        double[] m_Weights;
        double m_Bias;

        double m_WeightedSum;
        double m_Output;
        public double Output { get => m_Output; } //Using properties because this value will be used at the Program class

        public Neuron() {}

        //The 2 overloaded methods below SetNeuron() are used as 'fake constructors' to just create two neurons in memory and to reuse them instead of allocating lots of memory to neurons that will only be used once at the execution of the Feed Forward stage

        //This method takes an array of doubles as input because it receives the converted array of inputs from the train file
        public void SetNeuron(double[] inputs, double[] weights, double bias) 
        {
            m_Inputs = inputs;
            m_Weights = weights;
            m_Bias = bias;

            CalcNeuronOutput();
        }

        //This method takes a list of doubles because it receives the hidden layer outputs which are saved in a list that is the input of the output layer
        public void SetNeuron(List<double> inputs, double[] weights, double bias)
        {
            m_Inputs = new double[inputs.Count];
            inputs.CopyTo(m_Inputs);
            m_Weights = weights;
            m_Bias = bias;

            CalcNeuronOutput();
        }

        //This function calls the necessary calculations to produce the neuron output, first the weighted sum and then the activation, used sigmoid function to activate the neuron
        void CalcNeuronOutput()
        {
            m_WeightedSum = WeightedSum(m_Inputs, m_Weights);
            m_Output = SigmoidActivation(m_WeightedSum);
        }

        double WeightedSum(double[] inputs, double[] weights)
        {
            double sum = 0.0;

            for (int i = 0; i < inputs.Length; ++i)
            {
                sum += inputs[i] * weights[i];
            }

            sum += m_Bias;

            return sum;
        }

        double SigmoidActivation(double weightedSum)
        {
            return 1.0 / (1.0 + Math.Exp(-weightedSum));
        }

        //This function classifies the flower according to its output, it considers the range between 0.0 and 1.0
        public double Result(double output)
        {
            if (output < 0.33)
            {
                return 0.0; //flower is an iris-setosa
            }
            if (output > 0.67)
            {
                return 1.0; //flower is an iris-virginica
            }
            else
            {
                return 0.5; //flower is an iris-versicolor
            }
        }
    }
}