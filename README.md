# Iris Flower with Backpropagation in C#

This project contains the implementation of a neural network that learns the iris flower problem in C# using the backpropagation algorithm. See the topics below for better understanding:
- This project was implemented with focus on solving the iris database problem, so its architecture was written based on that
- The const values of some variables are functional values, if you wish, you can change those values to see the different results you can achieve
- The neural network doesn't have 100% precision, you may execute a case where you achieve that but this doesn't happen usually. By the way, the precision isn't far so bad, keeping the average range of ~60% to 70%
- Almost every step in the code is commented to be clear for the developer and user of the project what's happening

Feel free to use this project for any purposes! 

### Solution developed by Rodrigo de Jesus Emerick, Technologist in Digital Games